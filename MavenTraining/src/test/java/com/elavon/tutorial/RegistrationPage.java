package com.elavon.tutorial;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

public class RegistrationPage {

	public WebDriver openApplication(String browser, String url) {

		WebDriver driver = null;

		if ("Chrome".equals(browser)) {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Glenny\\git\\seleniumbasics\\testMavenJunit\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else {
			System.setProperty("webdriver.gecko.driver",
					"C:\\Glenny\\git\\seleniumbasics\\testMavenJunit\\drivers\\geckodriver_32.exe");
			driver = new FirefoxDriver();
		}

		driver.get(url);

		if ("Chrome".equals(browser)) {
			driver.manage().window().maximize();
		}

		return driver;
	}

	@Test
	public void testScript01() throws InterruptedException {

		// 01- Launch Google
		String browser = "Chrome";
		String url = "http://www.google.com";
		WebDriver driver = openApplication(browser, url);

		// 02- Signin

		WebElement Signin = driver.findElement(By
				.cssSelector(".gb_6f > div:nth-child(1)"));
		Signin.click();

		// // 03 - More Options
		WebElement MoreOptions = driver
				.findElement(By
						.xpath("//*[@id='view_container']/form/div[2]/div/div[2]/div[2]/div"));
		//
		MoreOptions.click();

		// 04 - Create Account

		WebElement Caccount = driver.findElement(By
				.xpath("//*[@id='SIGNUP']/div"));
		// int i = 0;
		// do {
		//
		Thread.sleep(3000);
		Caccount.click();

		// Thread.sleep(5000);
		// System.out.println(driver.getTitle());
		// System.out.println("counter:" + i);
		// System.out.println("boolean: "+
		// "Create your Google Account".equals(driver.getTitle()));
		// i++;
		// } while
		// ("Create your Google Account".equalsIgnoreCase(driver.getTitle()));

		// 05 - First Name
		Thread.sleep(3000);
		WebElement firstNameTextbox = driver.findElement(By.id("FirstName"));
		String firstName = "Glenny";
		typeInto(firstNameTextbox, firstName); //
		// Last Name
		WebElement lastNameTextbox = driver.findElement(By.id("LastName"));
		String lastName = "Jarin";
		typeInto(lastNameTextbox, lastName);

		// User Name
		WebElement userNameTextbox = driver.findElement(By.id("GmailAddress"));
		String userName = "gjarin302";
		typeInto(userNameTextbox, userName);

		// Password
		WebElement PwdTextbox = driver.findElement(By.id("Passwd"));
		String pwd = "Abchjk123";
		typeInto(PwdTextbox, pwd);

		// Confirm Password
		WebElement PwdAgain = driver.findElement(By.id("PasswdAgain"));
		String pwdagain = "Abchjk123";
		typeInto(PwdAgain, pwdagain);

		// BirthMonth

		WebElement Birthmonth = driver.findElement(By
				.xpath("//*[@id='BirthMonth']/div"));
		Birthmonth.sendKeys("January");
		// BirthDay
		WebElement Birthday = driver.findElement(By.id("BirthDay"));
		String birthday = "01";
		typeInto(Birthday, birthday);

		// BirthYear

		WebElement Birthyear = driver.findElement(By.id("BirthYear"));
		String birthyear = "2000";
		typeInto(Birthyear, birthyear);

		// Gender
		WebElement Gender = driver.findElement(By
				.xpath("//*[@id='Gender']/div[1]"));

		Gender.sendKeys("Female");

		// Mobile Phone

		WebElement PhoneNum = driver.findElement(By.id("RecoveryPhoneNumber"));
		String phonenum = "9998526363";
		typeInto(PhoneNum, phonenum);
	
		// Email Address

		WebElement EmailAdd = driver.findElement(By.id("RecoveryEmailAddress"));
		String emailadd = "a@yahoo.com";
		typeInto(EmailAdd, emailadd);

		// Location
		WebElement CountryCode = driver.findElement(By
				.xpath("//*[@id='CountryCode']/div[1]"));

		// String countrycode = "Argentina";
		CountryCode.sendKeys("Argentina");

		// Next Steps
		WebElement Nextbutton = driver.findElement(By
				.xpath("//*[@id='submitbutton']"));
		Thread.sleep(5000);
		Nextbutton.click();

		// driver.close();
	}

	public void typeInto(WebElement element, String value) {
		element.clear();
		element.sendKeys(value);
	}
}
