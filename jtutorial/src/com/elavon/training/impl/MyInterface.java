package com.elavon.training.impl;

import com.elavon.training.AwesomeCalculator;

public class MyInterface implements AwesomeCalculator {

	static int valDiff, fahrenheit, celcius;
	static double valProd;
	static int valDiv;
	static int dividend;
	static int minuend;
	static int subtrahend;
	static int divisor;
	static int remainderDiv;
	static String str, strname;
	static char palindrome;
	static double multiplicand;
	static double multiplier, valCelcius, varfahrenheit, varkilo, lbs, kg,
			varpound;
	static char reverse;
	static String reverseval;

	public String toString() {

		return "Difference of " + minuend + " and " + subtrahend + " is "
				+ valDiff + "\n" + "Product of " + multiplicand + " and "
				+ multiplier + " is " + valProd + "\n"
				+ "getQuotientAndRemainder " + "(" + dividend + "," + divisor
				+ ")" + " returns " + valDiv + " remainder " + remainderDiv
				+ "\n" + "toCelcius" + "(" + fahrenheit + ")" + "=>"
				+ valCelcius + "\n" + "tofahrenheit" + "(" + celcius + ")"
				+ "=>" + varfahrenheit + "\n" + "toKilogram" + "(" + lbs + ")"
				+ "=>" + varkilo + "=>" + varpound + "\n" + "toPounds" + "("
				+ kg + ")" + "=>" + varpound + "\n" + "Palindrome of " + str
				+ " is " + reverseval + "=>" + isPalindrome(str);

	}

	public static void main(String[] args) {

		AwesomeCalculator myinterface = new MyInterface();
		myinterface.getDifference(5, 10);
		myinterface.getProduct(1, 6);
		myinterface.getQuotientAndRemainder(17, 5);
		myinterface.isPalindrome("river");
		myinterface.toCelsius(68);
		myinterface.toFahrenheit(20);
		myinterface.toKilogram(51);
		myinterface.toPound(112.2);
		System.out.println(myinterface.toString());

	}

	@Override
	public int getSum(int augend, int addend) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getDifference(int minuend, int subtrahend) {
		MyInterface.minuend = minuend;
		MyInterface.subtrahend = subtrahend;
		valDiff = subtrahend - minuend;
		return valDiff;
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		MyInterface.multiplicand = multiplicand;
		MyInterface.multiplier = multiplier;
		valProd = multiplicand * multiplier;
		return valProd;
	}

	@Override
	public int getQuotientAndRemainder(int dividend, int divisor) {
		valDiv = dividend / divisor;
		remainderDiv = dividend % divisor;
		MyInterface.dividend = dividend;
		MyInterface.divisor = divisor;
		return valDiv;
	}

	@Override
	public double toCelsius(int fahrenheit) {
		valCelcius = (fahrenheit - 32) * 5 / 9;
		MyInterface.fahrenheit = fahrenheit;
		return valCelcius;
	}

	@Override
	public double toFahrenheit(int celsius) {
		varfahrenheit = ((celsius * 9) / 5) + 32;
		MyInterface.celcius = celsius;
		return varfahrenheit;
	}

	@Override
	public double toKilogram(double lbs) {
		varkilo = lbs * 2.2;
		MyInterface.lbs = lbs;
		return varkilo;
	}

	@Override
	public double toPound(double kg) {
		varpound = kg / 2.2;
		MyInterface.kg = kg;
		return varpound;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.elavon.training.AwesomeCalculator#isPalindrome(java.lang.String)
	 */
	@Override
	public boolean isPalindrome(String str) {
		MyInterface.str = str;
		String reversal = "";
		int length = str.length();
		int i;
		char reverse;
		for (i = length - 1; i >= 0; --i) {
			reverse = str.charAt(i);
			reversal = reversal + reverse;
			MyInterface.reverseval = reversal;
		}
		// if (reversal.equals(str) ) {
		// return true;
		//
		// } else {
		// return false;
		// }

		return reversal.equals(str);
	}
}
