package com.elavon.jtutorial.ex6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import javax.swing.JOptionPane;

public class LeaveAmessage {

	public static void main(String[] args) {
		SendMessage();

	}

	private static void SendMessage() {
		Path path = Paths.get("C:/", "Users", "Gvjarin", "message.txt");
		try (BufferedReader reader = Files.newBufferedReader(path);
				BufferedWriter writer = Files.newBufferedWriter(path,
						StandardOpenOption.APPEND)) {

			String str = null;
			String username = JOptionPane.showInputDialog("Login:");

			String recepient = JOptionPane.showInputDialog("To:");
			String messagetext = JOptionPane.showInputDialog("Message:");

			writer.newLine();
			writer.write(username + ":" + recepient + ":" + "<" + messagetext
					+ ">");
			writer.newLine();
			while ((str = reader.readLine()) != null) {
				if (str.contains(recepient)) {

					JOptionPane.showMessageDialog(null, str, "Messages",
							JOptionPane.PLAIN_MESSAGE);

				}
			}

		} catch (IOException e) {
			System.err.println("File not found!");
		}

	}

	public static void paths() {
		Path path = Paths.get("C:/", "Users", "Gvjarin", "myFile.txt");
		System.out.println(Files.exists(path, LinkOption.NOFOLLOW_LINKS));
	}

}
