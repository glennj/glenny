package com.elavon.jtutorial.ex2;

class TV {
	protected String brand;
	protected String model;
	protected boolean power;
	protected int channel;
	protected int volume;

	TV() {
		
		power = false;
		channel = 0;
		volume = 5;
		brand = "null";
		model = "null";
	}

	public String toString() {

	
		return "Power=" + power + "," + "Channel=" + channel + "," + "Brand="
				+ brand + "," + "Model=" + model + "," + "Volume=" + volume;

	}

	public static void main(String[] args) {
		TV tv = new TV();
		tv.brand = "Andre Electronics";
		tv.model = "One";
		System.out.println(tv.toString());
		tv.turnOn();
		int i = 5;
		for (int num = 0; num < i; num++) {
			tv.channelUp();
		}

		tv.channelDown();
		int j = 3;
		for (int num1 = 0; num1 < j; num1++) {
			tv.volumeDown();
		}

		tv.volumeUp();
		
		tv.turnOff();
		System.out.println(tv.toString());
	
	}

	void turnOn() {

		power = true;

	}

	void turnOff() {
		power = false;
	}

	void channelUp() {
		channel++;
	}

	void channelDown() {
		channel--;
	}

	void volumeUp() {
		volume++;
	}

	void volumeDown() {
		volume--;
	}

}
