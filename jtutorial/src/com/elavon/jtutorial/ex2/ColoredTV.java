package com.elavon.jtutorial.ex2;

class ColoredTV extends TV {
	private int brightness;
	private int contrast;
	private int picture;


	ColoredTV() {
		
		brightness = 50;
		contrast = 50;
		picture = 50;
	
		
	}

//	overrides toString
	public  String toString(int channel){
		 return super.toString() ;

	}
	
	public static void main(String[] args) {
		
		ColoredTV bnwTV = new ColoredTV();
		ColoredTV sonyTV = new ColoredTV();

		bnwTV.brand = "Admiral";
		bnwTV.model="A1";
		sonyTV.brand="Sony";
		sonyTV.model="S1";
		
		System.out.println(sonyTV.toString() + "," + "Brightness=" +sonyTV.brightness + ","  + "Contrast=" + sonyTV.contrast + "," + "Picture=" + sonyTV.picture );

		sonyTV.brightnessUp();
		System.out.println(sonyTV.toString() + "," + "Brightness=" +sonyTV.brightness + ","  + "Contrast=" + sonyTV.contrast + "," + "Picture=" + sonyTV.picture);
		ColoredTV sharpTV = new ColoredTV();
		sharpTV.brand = "Sharp";
		sharpTV.model="SH1";
		sharpTV.mute();
		sharpTV.brightnessUp();
		sharpTV.contrastUp();
		sharpTV.pictureUp();
		//sharpTV.channel=5;
		sharpTV.switchToChannel(3);
		System.out.println(sharpTV.toString() + "," + "Brightness=" +sharpTV.brightness + ","  + "Contrast=" + sharpTV.contrast + "," + "Picture=" + sharpTV.picture );
	}

	void brightnessUp() {
		brightness++;
	}
	
	void contrastUp()
	{
		contrast++;
	}
	
	void pictureUp()
	{
		picture++;
	}
	
	void switchToChannel(int newchannel)
	{
	channel=newchannel;

	}
	
	void mute()
	{
		volume = 0;
				
	}
	

}
